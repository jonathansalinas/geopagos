package pruebaGeoPagos.dtos;

import lombok.*;

import javax.validation.constraints.NotBlank;

@Getter @Setter
@AllArgsConstructor @NoArgsConstructor
@ToString
public class FiguraDTO {
    @NotBlank(message = "Tipo no puede ser nulo")
    private String tipo;
    private Double base;
    private Double altura;
    private Double diametro;
}
