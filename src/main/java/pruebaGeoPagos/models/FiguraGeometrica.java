package pruebaGeoPagos.models;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public interface FiguraGeometrica {

    String getTipo();

    Double getSuperficie();

    Double getBase();

    Double getAltura();

    default Double getDiametro() {
        return null;
    }
}
