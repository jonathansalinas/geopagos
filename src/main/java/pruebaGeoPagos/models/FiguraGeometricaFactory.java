package pruebaGeoPagos.models;

import pruebaGeoPagos.dtos.FiguraDTO;

public class FiguraGeometricaFactory {

    public static FiguraGeometrica getInstancia(FiguraDTO figuraDTO) {
        String tipoFigura = figuraDTO.getTipo().toUpperCase();
        FiguraGeometrica figura = null;
        switch (tipoFigura) {
            case TipoFigura.CIRCULO:
                figura = new Circulo(figuraDTO.getDiametro());
                break;

            case TipoFigura.CUADRADO:
                figura = new Cuadrado(figuraDTO.getBase(), figuraDTO.getAltura());
                break;

            case TipoFigura.TRIANGULO:
                figura = new Triangulo(figuraDTO.getBase(), figuraDTO.getAltura());
                break;

            default: break;

        }
        return figura;
    }

}
