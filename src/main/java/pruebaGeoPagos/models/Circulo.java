package pruebaGeoPagos.models;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class Circulo implements FiguraGeometrica {

    private Double diametro;

    @Override
    public String getTipo() {
        return TipoFigura.CIRCULO;
    }

    @Override
    public Double getSuperficie() {
        return Math.PI * Math.pow(this.diametro / 2, 2);
    }

    @Override
    public Double getBase() {
        return null;
    }

    @Override
    public Double getAltura() {
        return null;
    }

    @Override
    public Double getDiametro() {
        return this.diametro;
    }

    public void setDiametro(Double diametro) {
        this.diametro = diametro;
    }
}
