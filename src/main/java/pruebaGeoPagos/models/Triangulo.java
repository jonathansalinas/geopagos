package pruebaGeoPagos.models;

import lombok.Builder;

public class Triangulo extends Poligono {

    @Builder
    public Triangulo(Double base, Double altura) {
        super(base, altura);
    }

    @Override
    public String getTipo() {
        return TipoFigura.TRIANGULO;
    }

}
