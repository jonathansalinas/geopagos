package pruebaGeoPagos.models;

public class TipoFigura {
    public final static String CIRCULO = "CIRCULO";
    public final static String CUADRADO = "CUADRADO";
    public final static String TRIANGULO = "TRIANGULO";
}
