package pruebaGeoPagos.models;

import lombok.Builder;

public class Cuadrado extends Poligono{

    @Builder
    public Cuadrado(Double base, Double altura) {
        super(base, altura);
    }

    @Override
    public String getTipo() {
        return TipoFigura.CUADRADO;
    }

}
