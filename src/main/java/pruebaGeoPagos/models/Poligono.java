package pruebaGeoPagos.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter @Setter
public abstract class Poligono implements FiguraGeometrica {

    private Double base;
    private Double altura;

    @Override
    public Double getSuperficie() {
        return this.base * this.altura;
    }

}
