package pruebaGeoPagos.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import pruebaGeoPagos.dtos.FiguraDTO;
import pruebaGeoPagos.models.FiguraGeometrica;
import pruebaGeoPagos.services.FiguraGeometricaService;

import javax.validation.Valid;
import java.util.List;

@RestController()
@RequestMapping("/figuras")
public class FiguraGeometricaController {

    @Autowired
    private FiguraGeometricaService figuraGeometricaService;
    private static final Logger logger = LoggerFactory.getLogger(FiguraGeometricaController.class);

    @GetMapping()
    public List<FiguraGeometrica> getFiguras() {
        return figuraGeometricaService.obtenerTodas();
    }

    @PostMapping()
    public FiguraGeometrica crearFigura(@RequestBody @Valid FiguraDTO figuraDTO) {
        try {
            return figuraGeometricaService.crearFigura(figuraDTO);
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);
        }
    }

}
