package pruebaGeoPagos.services;

import org.springframework.stereotype.Service;
import pruebaGeoPagos.dtos.FiguraDTO;
import pruebaGeoPagos.models.FiguraGeometrica;

import java.util.List;

@Service
public interface FiguraGeometricaService {

    public List<FiguraGeometrica> obtenerTodas();

    FiguraGeometrica crearFigura(FiguraDTO figuraDTO) throws Exception;

}
