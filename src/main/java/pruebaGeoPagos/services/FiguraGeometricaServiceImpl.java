package pruebaGeoPagos.services;

import org.springframework.stereotype.Service;
import pruebaGeoPagos.dtos.FiguraDTO;
import pruebaGeoPagos.models.FiguraGeometrica;
import pruebaGeoPagos.models.FiguraGeometricaFactory;
import pruebaGeoPagos.models.TipoFigura;

import java.util.ArrayList;
import java.util.List;

@Service
public class FiguraGeometricaServiceImpl implements FiguraGeometricaService {

    List<FiguraGeometrica> figuras = new ArrayList<>();

    @Override
    public List<FiguraGeometrica> obtenerTodas() {
        return this.figuras;
    }

    @Override
    public FiguraGeometrica crearFigura(FiguraDTO figuraDTO) throws Exception {
        validarCreacion(figuraDTO);
        FiguraGeometrica figura = FiguraGeometricaFactory.getInstancia(figuraDTO);
        figuras.add(figura);
        return figura;
    }

    private void validarCreacion(FiguraDTO figuraDTO) throws Exception {
        if(figuraDTO.getTipo().equalsIgnoreCase(TipoFigura.CIRCULO) && figuraDTO.getDiametro() == null) {
            throw new Exception("Para un circulo, el diametro tiene que estar definido");
        }
        if (!figuraDTO.getTipo().equalsIgnoreCase(TipoFigura.CIRCULO) && (figuraDTO.getAltura() == null || figuraDTO.getBase() == null)) {
            throw new Exception("Para un triangulo o cuadrado, los campos base y altura son obligatorios");
        }
    }

}
